# Introduction

## Installation

```bash
pip install luucy-python
```

## Usage

### Updating custom properties for 3D building

```python
from luucy import BaseClient, Attribute

client = BaseClient(username="<USERNAME>", password="<PASSWORD>")

Attribute(client, attribute_id=27).update_value(
  uuid="fa8e48e4-bed6-4215-ae01-18fe23155ce3", value="1"
)

```

## Objects

- [`tilesetproxy.Attribute`](reference/api.html#luucy.tilesetproxy.Attribute) - interact with TilesetProxy attributes
