# Luucy Python

Official python package to interact with [LUUCY](https://www.luucy.ch).

## Installation

The package can be installed via pip:

```bash
pip install luucy-python
```
